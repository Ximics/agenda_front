import React from 'react';
import './Menu.scss';
import {NavLink} from "react-router-dom";

export default function Menu() {

    return(
        <nav className="MenuContainer">
                <NavLink className="MenuContainer__inputs" to="/login">INICIAR SESIÓN</NavLink>
                <NavLink className="MenuContainer__inputs" to="/">CERRAR SESIÓN</NavLink>
        </nav>
    )
}