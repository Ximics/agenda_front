import React from "react";
import IndexPage from "../Pages/IndexPage/IndexPage";
import {Route, Switch} from "react-router-dom";
import Login from "../Pages/Login/Login";
import Edit from "../Pages/IndexPage/Details/Crud/Edit/Edit";

export default function Routes(){

    return(
        <Switch>
            <Route strict exact path="/">
                <IndexPage></IndexPage>
            </Route>
            <Route strict exact path="/login">
                <Login></Login>
            </Route>
            <Route strict exact path="/edit">
                <Edit></Edit>
            </Route>
        </Switch>
    )
}