import React from 'react';
import List from "./List/List";
import Details from "./Details/Details";
import './IndexPage.scss'

export default function IndexPage(){


    return(
        <div className="IndexContainer">
            <List></List>
            <Details></Details>
        </div>
)
}