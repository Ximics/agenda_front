import React from 'react';
import './Details.scss';
import Crud from "./Crud/Crud";

export default function Details(){


    return(
        <div className="DetailsContainer">
            <h1>Detalles del contacto</h1>
           <div className="DetailsContainer__box">
                   <div className="DetailsContainer__box__details">
                       <h3>Nombre :</h3>
                       <h3>Mario</h3>

                   </div>
                   <div className="DetailsContainer__box__details">
                        <h3>Apellidos :</h3>
                        <h3>Cantero Shimizu</h3>
                   </div>

                   <div className="DetailsContainer__box__details">
                       <h3>Calle :</h3>
                       <h3>Me suda los cojones</h3>
                   </div>

                   <div className="DetailsContainer__box__details">
                       <h3>Ciudad :</h3>
                       <h3>Betis</h3>
                   </div>

                   <div className="DetailsContainer__box__details">
                       <h3>Código Postal :</h3>
                       <h3>69</h3>
                   </div>

                   <div className="DetailsContainer__box__details">
                       <h3>Cumpleaños :</h3>
                       <h3>11-m</h3>
                   </div>
               </div>
            <div className="CrudSize">
                <Crud></Crud>
            </div>
           </div>
    )
}