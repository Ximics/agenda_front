import React from 'react';
import './Edit.scss';
import {NavLink} from "react-router-dom";
import {useForm} from "react-hook-form";

export default function Edit(){
    const {register, handleSubmit} = useForm()
    const onSubmit = (data) =>{
        console.log(data)
    }

    return(
        <form onSubmit={handleSubmit(onSubmit)} className="EditMainContainer">
            <div className="EditContainer">
                <div className="EditContainer__static">
                    <p>Nombre</p>
                    <p>Apellidos</p>
                    <p>Calle</p>
                    <p>Ciudad</p>
                    <p>Código Postal</p>
                    <p>Fecha de nacimiento</p>
                </div>

                <div className="EditContainer__input">
                    <input className="EditContainer__input__box" type="text" name="nombre" ref={register({required : true})} />
                    <input className="EditContainer__input__box" type="text" name="apellidos" ref={register({required : true})} />
                    <input className="EditContainer__input__box" type="text" name="calle" ref={register({required : true})} />
                    <input className="EditContainer__input__box" type="text" name="ciudad" ref={register({required : true})}/>
                    <input className="EditContainer__input__box" type="text" name="codigo postal" ref={register({required : true})}/>
                    <input className="EditContainer__input__box" type="text" name="fecha de nacimiento" ref={register({required : true})}/>
                </div>
            </div>

            <div className="editSubmit">
                <input className="editSubmit__submit" type="submit" value="OK"/>
                <NavLink className="editSubmit__cancel" to="/">CANCELAR</NavLink>
            </div>
        </form>

    )

}