import React from 'react';
import './Crud.scss'
import {NavLink} from "react-router-dom";
export default function Crud(){
    return(
        <nav className="CrudContainer">
            <NavLink className="CrudContainer__inputs" to="/"><p>NEW</p></NavLink>
            <NavLink className="CrudContainer__inputs" to="/edit">EDIT</NavLink>
            <NavLink className="CrudContainer__inputs" to="/" >DELEte</NavLink>
        </nav>
    )
}