
import './App.scss';
import Menu from "./Core/Menu/Menu";
import {BrowserRouter as Router} from "react-router-dom";
import Routes from "./Routes/Routes";

function App() {
  return (
      <Router>
          <div className="container">
              <Menu></Menu>
              <Routes></Routes>
          </div>
      </Router>
  );
}

export default App;
